import os

from PIL import Image

def rename(directory):
    for file in os.listdir(directory):
        old_file = file.split('.')
        new_file_name = "{0}.{1}".format(old_file[0], old_file[-1])
        os.rename(os.path.join(directory, file), os.path.join(directory, new_file_name))

def convert2jpg(directory):
    for file in os.listdir(directory):
        if file.endswith('png') or file.endswith('gif'):
            img = Image.open(os.path.join(directory, file))
            rgb_img = img.convert('RGB')
            file_name = file.split('.')[0]
            rgb_img.save(os.path.join(directory, '{0}.{1}'.format(file_name, 'jpg')))
            os.remove(os.path.join(directory, file))

def resize_images(directory_in, directory_out):
    for file in os.listdir(directory_in):
        try:
            img = resize_image(os.path.join(directory_in, file))
            img.save(os.path.join(directory_out, file))
        except OSError:
            print('file {} cant resize'.format(file))

def resize_image(file, size=(300, 300)):
    img = Image.open(file)
    img = img.resize(size, Image.ANTIALIAS)
    return img

if __name__ == '__main__':
    #rename('hard_hat')
    #convert2jpg('hard_hat')
    resize_images('hard_hat', 'images')
